Source: conntrack-tools
Section: net
Priority: optional
Maintainer: Debian Netfilter Packaging Team <pkg-netfilter-team@lists.alioth.debian.org>
Uploaders: Arturo Borrero Gonzalez <arturo@debian.org>, Alexander Wirt <formorer@debian.org>
Homepage: http://conntrack-tools.netfilter.org/
Build-Depends: bison,
               debhelper (>= 11),
               flex,
               libmnl-dev (>= 1.0.1),
               libnetfilter-conntrack-dev (>= 1.0.7),
               libnetfilter-cthelper0-dev,
               libnetfilter-cttimeout-dev (>= 1.0.0),
               libnetfilter-queue-dev (>= 1.0.2),
               libnfnetlink-dev (>= 1.0.1),
	       libsystemd-dev (>= 227),
	       autoconf, automake, libtool
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/pkg-netfilter-team/pkg-conntrack-tools
Vcs-Browser: https://salsa.debian.org/pkg-netfilter-team/pkg-conntrack-tools

Package: conntrack
Architecture: linux-any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: nftables
Description: Program to modify the conntrack tables
 conntrack is a userspace command line program targeted at system
 administrators. It enables them to view and manage the in-kernel
 connection tracking state table.

Package: conntrackd
Architecture: linux-any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: nftables
Description: Connection tracking daemon
 Conntrackd can replicate the status of the connections that are
 currently being processed by your stateful firewall based on Linux.
 Conntrackd can also run as statistics daemon.

Package: nfct
Architecture: linux-any
Depends: conntrackd (>= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Suggests: nftables
Description: Tool to interact with the connection tracking system
 nfct is the command line tool that allows you to manipulate the Netfilter's
 Connection Tracking System.
 .
 By now, the supported subsystem is timeout.
